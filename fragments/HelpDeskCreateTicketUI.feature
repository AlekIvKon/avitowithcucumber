#language:ru
Функционал: фрагмент

  @fragment
  Сценарий: создание тикета на странице HelpDesk

    * шаг № "1"
    * открыть url "https://at-sandbox.workbench.lanit.ru"
    * инициализация страницы "HelpDesk"
    * кликнуть на элемент "кнопка Создать тикет"
    * инициализация страницы "CreateTicketPage"
    * сгенерировать переменные
      | id              | 0                 |
      | title           | RRRRRRRRRRRRR     |
      | submitter_email | EEEEEEEDDD@EEE.EE |
      | status          | 1                 |
      | priority        | 1                 |
      | queue           | 1                 |

    * выбрать в всплывающем списке Queue строку "1".
    * выбрать в всплывающем списке Priority строку "1".
    * ввести в поле "title" значение "${title}"
    * ввести в поле "submitter_Email" значение "${submitter_email}"
    * кликнуть на элемент "календарь даты создания"
    * кликнуть на элемент "выбрать день"    
    * кликнуть на элемент "Submit Ticket Button"
    * инициализация страницы "ViewPage"
    * сохранить текст из поля "priority_field" под именем "web_priority_field"
    * сохранить текст из поля "поле емэйл" под именем "web_submit_email"
    * сохранить текст из поля "поле содержащее title" под именем "web_title"
    * сравнить значения
    |${web_priority_field}|содержит |${priority}|
    |${web_submit_email}|содержит |${submitter_email}|
    |${web_title}|содержит |${title}|

    

    


