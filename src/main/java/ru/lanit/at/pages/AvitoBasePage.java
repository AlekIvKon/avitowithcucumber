package ru.lanit.at.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.lanit.at.utils.web.annotations.Name;
import ru.lanit.at.utils.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$;

@Name(value = "Стартовая страница авито")
public class AvitoBasePage extends WebPage {

    @Name("категория Мотоциклы и мототехника")
    private SelenideElement searchField = $(By.xpath("//a[@data-name='Мотоциклы и мототехника']"));

    @Name("кнопка выбрать категорию")
    private SelenideElement buttonAllCategories = $(By.xpath("//button[@data-marker='top-rubricator/all-categories']"));

    @Name("кнопка выбрать место поиска")
    private SelenideElement buttonSelectRegion = $(By.xpath("//div[@data-marker='search-form/region']"));

    @Name("поле выбора региона")
    private SelenideElement inputSelectedRegion = $(By.xpath("//input[@placeholder=\"Город или регион\"]"));

    @Name("кнопка сохранить локацию")
    private SelenideElement save_button = $(By.xpath("//button[@data-marker='popup-location/save-button']"));





}