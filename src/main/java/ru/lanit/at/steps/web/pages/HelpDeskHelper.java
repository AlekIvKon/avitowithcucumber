package ru.lanit.at.steps.web.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.Если;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Когда;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.lanit.at.actions.WebActions;
import ru.lanit.at.steps.web.AbstractWebSteps;
import ru.lanit.at.utils.Sleep;
import ru.lanit.at.utils.web.pagecontext.PageManager;

import java.io.File;
import java.nio.file.Path;
import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static ru.lanit.at.utils.VariableUtil.replaceVars;


public class HelpDeskHelper extends AbstractWebSteps {

    public HelpDeskHelper(PageManager pageManager) {
        super(pageManager);
    }
    @И("загрузить  файл {string} на сайт:")
    public void uploadFile(String path) {
        $x("//input[@id='file0']").uploadFile(new File(path));
    }

    @И("выбрать в всплывающем списке Sorting строку {string}.")
    public void select(String string){
       $x("//select[@id='id_sortx']").selectOptionByValue(string);
    }
    @И("выбрать в всплывающем списке Queue строку {string}.")
    public void selectQueue(String string){
        $x("//select[@id='id_queue']").selectOptionByValue(string);
    }
    @И("выбрать в всплывающем списке Priority строку {string}.")
    public void selectPriority(String string){
        $x("//select[@id='id_priority']").selectOptionByValue(string);
    }




    @И("выбрать в всплывающем списке Status строку {string}.")
    public void selectStatus(String string){
        $x("//select[@id='id_statuses']").selectOptionByValue(string);
    }



}
